﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Valarep___TP_01___Monster_Arena
{
    public partial class FrmFight : Form
    {
        // todo 07 : Déclarer une propriété publique Images 
        //           Cette propriété est est un tableau de type Image
        //           Utiliser la déclaration des méthodes get et set par défaut
        //           avec la méthode set privée

        // todo 08 : Déclarer les champs privés monstre1 et monstre2 de type Monstre

        public FrmFight()
        {
            InitializeComponent();
        }

        private void FrmCombat_Load(object sender, EventArgs e)
        {
            // todo 09 : Déclarer une variable de type FrmLoad appelée chargement

            // todo 10 : Instancier la fenêtre chargement;

            // todo 11 : Utiliser la méthode ShowDialog de la fenêtre chargement 
            //           pour afficher la fenêtre en mode "Boîte de Dialogue"

            // todo 12 : Affecter la propriété Images de la fenêtre chargement
            //           à la propriété Images de cette fenêtre

        }

        private void btnChoisirJ1_Click(object sender, EventArgs e)
        {
            // todo 13 : Déclarer une variable choixDuMonstre de type FrmSelectMonster

            // todo 14 : Instancier choixDuMonstre
            // todo 15 : Affecter le tableau Images à la propriété Images de choixDuMonstre

            // todo 16 : Utiliser la méthode ShowDialog de la fenêtre choixDuMonstre
            //           pour afficher la fenêtre en mode "Boîte de Dialogue"

            // todo 17 : Affecter à monstre1 la valeur fournie par la propriété SelectedMonster de la variable choixDuMonstre

            // todo 18 : Affecter à picMonstre1 l'image du monstre
            // todo 19 : Affecter à progressBarMonstre1 les points de vie du monstre

        }

        private void btnChoisirJ2_Click(object sender, EventArgs e)
        {
            // todo 20 : Faire le même travail que pour btnChoisirJ1_Click
            //           mais avec le monstre2

        }

        private void btnAttaque1J1_Click(object sender, EventArgs e)
        {
            // todo 21 : Utiliser l'attaque 1 du monstre 1
            //           pour réduire la vie du monstre 2
            //           attention, la vie d'un monstre n'a pas le droit d'être négative

            // todo 22 : Actualiser la ProgressBar du Monstre 2

            // todo 23 : Afficher le nom de l'attaque et les dommages causés

            // todo 24 : si le niveau de vie du monstre2 atteint 0
            //           afficher un message annonçant la victoire du joueur 1

        }

        private void btnAttaque2J1_Click(object sender, EventArgs e)
        {
            // todo 25 : Faire le même travail que dans btnAttaque1J1_Click
            //           mais avec l'attaque 2

        }

        private void btnAttaque1J2_Click(object sender, EventArgs e)
        {
            // todo 26 : Faire le même travail que dans btnAttaque1J1_Click
            //           mais avec l'attaque 1 du monstre 2 pour attaquer le montre 1

        }

        private void btnAttaque2J2_Click(object sender, EventArgs e)
        {
            // todo 27 : Faire le même travail que dans btnAttaque1J2_Click
            //           mais avec l'attaque 2

        }

        // Todo 28 : [Bonus] Gestion de l'expérience sur les attaques
        // Ajouter la notion d'expérience du montre pour chacune de ses deux attaques
        // Cette expérience prendra la forme de propriétés dans la classe Monstre (qui sera accessible en lecture/écriture)
        // L'expérience admet des valeurs comprises entre 0 et 100
        // Plus un monstre à d'expérience dans une attaque, plus il aura de chance de la réussir
        // Au départ l'expérience d'un monstre dans une attaque est de 10
        // A chaque attaque, un test de l'expérience du monstre est effectué, sous forme d'un tirage au sort entre 1 et 100
        // Si le score du tirage au sort est supérieur à l'expérience, l'attaque a échoué et ne fera aucun dommage
        // Si le score est inférieur ou égal à l'expérience, l'attaque réussie,
        // dans ce cas l'expérience est augmentée de 10 et on applique les dommages prévus
    }
}
