﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Valarep___TP_01___Monster_Arena
{
    public class Monster
    {
        // todo 04 : Expliquer quelle est la différence entre life et Life

        private int life;
        public int Life
        {
            get { return life; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException("Life", value, "Invalid Life Points value (must be between 0 and 100).");
                }

                // todo 05 : la valeur est-elle systématiquement enregistrée dans le champ life ?

                life = value;
            }
        }

        public int Strength { get; set; }
        // todo 05 : Modifier la propriété publique Strength de type entier
        //           Cette propriété doit impérativement avoir une valeur comprise entre 0 et 100

        public Image Image { get; private set; }

        public Attack Attack1 { get; private set; }
        public Attack Attack2 { get; private set; }

        public static Monster Create(int index, Image image)
        {
            Monster monster;

            // todo 06 : Création des monstres en fonction de l'index et de l'image fournis
            //           Utiliser une structure de  contrôle conditionnelle
            //           pour créer le monstre en correspondant aux valeurs ci-dessous

            //           Monstre  1, 2, 3, 4 : Life =  50, Stength = 50, Attack1 = HornAttack, Attack2 = ClawAttack
            //           Monstre  5, 6, 7, 8 : Life =  70, Stength = 40, Attack1 = ClawAttack, Attack2 = BiteAttack
            //           Monstre  9,10,11,12 : Life =  40, Stength = 60, Attack1 = BiteAttack, Attack2 = EyesAttack
            //           Monstre 13,14,15,16 : Life = 100, Stength = 10, Attack1 = EyesAttack, Attack2 = HeadAttack
            //           Monstre 17,18,19,20 : Life =  80, Stength = 20, Attack1 = HeadAttack, Attack2 = MadnessAttack
            //           Monstre 21,22,23,24 : Life =  70, Stength = 45, Attack2 = MadnessAttack, Attack2 = HornAttack

            // Une fois les monstres créés, mettre les exemples en commentaire ou les supprimer

            // exemple de création d'un monstre avec la propriété Life à 0
            monster = new Monster() { Life = 0, };
            // exemple d'affectation d'une attaque à un monstre
            monster.Attack1 = Attack.HornAttack(monster.Strength);

            return monster;
        }
    }
}
